package com.bondarenko.artem.calculator;

import org.junit.Before;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

public class FunctionsTest {
    private Functions functions = mock(Functions.class);
    Function f = mock(Function.class);

    @Before
    public void setup() {
        functions.expressions = new HashMap<>();
    }

    @Test
    public void calculateGoodParamsTest() {
        doCallRealMethod().when(functions).calculate(anyString());

        functions.calculate("a=10 b=11");

        Map<String, Double> params = new HashMap<>();
        params.put("a", 10.0);
        params.put("b", 11.0);
        verify(functions).calculateWithParams(params);
    }

    @Test
    public void calculateBadParamsTest() {
        doCallRealMethod().when(functions).calculate(anyString());

        functions.calculate("a=10 b=c");

        Map<String, Double> params = new HashMap<>();
        params.put("a", 10.0);
        verify(functions).calculateWithParams(params);
    }

    @Test
    public void runCalculations() {
        doCallRealMethod().when(functions).calculateWithParams(anyMap());
        doCallRealMethod().when(f).init(anyString());
        doCallRealMethod().when(f).calculate(anyMap());
        doCallRealMethod().when(f).canBeCalculated(anySet());

        String func = "a+b;a,b";
        f.init(func);
        functions.expressions.put(func, f);

        Map<String, Double> params = new HashMap<>();
        params.put("a", 10.0);
        params.put("b", 11.0);
        functions.calculateWithParams(params);

        verify(f).canBeCalculated(params.keySet());
        verify(f).calculate(params);
    }
}