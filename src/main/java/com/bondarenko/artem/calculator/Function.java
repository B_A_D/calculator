package com.bondarenko.artem.calculator;

import net.objecthunter.exp4j.Expression;
import net.objecthunter.exp4j.ExpressionBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Collections;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class Function {
    private static Logger logger = LoggerFactory.getLogger(Function.class);

    protected Expression exp;
    private Set<String> parameters = Collections.emptySet();

    public Function(String func) {
        init(func);
    }

    protected void init(String func) {
        String[] funcAndVars = func.split(";");
        ExpressionBuilder expressionBuilder = new ExpressionBuilder(funcAndVars[0]);
        if (funcAndVars.length > 1)
            parameters = Stream.of(funcAndVars[1].split(",")).collect(Collectors.toSet());


        exp = expressionBuilder.variables(parameters).build();
    }

    public double calculate(Map<String, Double> parameters) {
        return exp.setVariables(parameters).evaluate();
    }

    public boolean canBeCalculated(Set<String> params) {
        return params.containsAll(parameters);
    }
}
