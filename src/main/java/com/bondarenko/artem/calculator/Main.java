package com.bondarenko.artem.calculator;

import org.apache.commons.cli.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.function.Supplier;
import java.util.stream.Stream;

public class Main {
    private static Logger logger = LoggerFactory.getLogger(Main.class);
    private static Functions functions = new Functions();
    private static String inp = "";
    private static Random rnd = new Random(System.currentTimeMillis());
    private static Map<String, Supplier<String>> suppliers = new HashMap<> ();

    public static void main(String[] args) {

        // get rid with options
        suppliers.put("static", Main::staticParametersSupplier);
        suppliers.put("random", Main::randomParametersSupplier);
        suppliers.put("input", Main::inputParametersSupplier);

        Options options = new Options();
        Option ffileOption = Option.builder()
                .argName("f")
                .longOpt("functions")
                .hasArg()
                .desc("Functions file path")
                .build();
        options.addOption(ffileOption);

        Option paramsOption = Option.builder()
                .argName("p")
                .longOpt("paramsMode")
                .hasArg()
                .desc("Mode for input parameters generator. Can be: 'static', 'random', 'input'")
                .build();
        options.addOption(paramsOption);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd = null;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("calculator", options);

            System.exit(1);
        }

        String ffile = cmd.getOptionValue("functions", "functions");
        String mode = cmd.getOptionValue("paramsMode", "input");

        if (!suppliers.containsKey(mode)) {
            logger.error("Invalid 'paramsMode' value: {}", mode);
            formatter.printHelp("calculator", options);
            System.exit(1);
        }

        // load expressions from file
        try {
            functions.loadFromFile(ffile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        // start to watch file with expressions
        FileWatcher monitor = new FileWatcher(ffile, functions);
        Thread fwThread = new Thread(monitor);
        fwThread.start();

        // start calculations using passed supplier
        Stream.generate(suppliers.get(mode))
                .forEach(s -> functions.calculate(s));
    }

    public static String staticParametersSupplier() {
        return "a=10 b=11";
    }

    public static String inputParametersSupplier() {
        Scanner scanner = new Scanner(System.in);
        String res = "";
        while(true) {
            int i = inp.indexOf(';');
            if(i != -1) {
                res = inp.substring(0, i);
                inp = inp.substring(i+1);
            }

            if (!res.isEmpty())
                return res;

            inp += scanner.next() + " ";
        }
    }

    public static String randomParametersSupplier() {
        String variables[] = {"a", "b", "c"};
        String res = "";

        for (int i = 0; i < variables.length; ++i)
            res += " " + variables[rnd.nextInt(variables.length)] + "=" + rnd.nextDouble() * 100;

        return res;
    }
}
