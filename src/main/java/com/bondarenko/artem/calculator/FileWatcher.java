package com.bondarenko.artem.calculator;

import java.io.IOException;
import java.nio.file.*;

public class FileWatcher implements Runnable {
    private final Path rootDir;
    private final String fileName;
    private final Functions functions;

    public FileWatcher(String file, Functions functions) {
        this.functions = functions;
        rootDir = Paths.get(file).getParent();
        fileName = Paths.get(file).getFileName().toString();
    }

    @Override
    public void run() {
        try {
            WatchService watchService
                    = FileSystems.getDefault().newWatchService();

            rootDir.register(
                    watchService,
                    StandardWatchEventKinds.ENTRY_MODIFY);

            WatchKey key;
            while ((key = watchService.take()) != null) {
                for (WatchEvent<?> event : key.pollEvents()) {
                    if (event.context().toString().equals(fileName)) {
                        System.out.println("Equals");
                        functions.loadFromFile(rootDir.toString() + "/" + fileName);
                    }
                }
                key.reset();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
