package com.bondarenko.artem.calculator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class Functions {

    private static Logger logger = LoggerFactory.getLogger(Functions.class);

    protected Map<String, Function> expressions = new HashMap<>();

    public void loadFromFile(String fileName) throws FileNotFoundException {
        logger.info("Load expressions from file {}", fileName);
        synchronized (expressions) {
            Map<String, Function> newExpressions = new HashMap<>();
            new BufferedReader(new InputStreamReader(new FileInputStream(fileName)))
                    .lines()
                    .forEach(l -> {
                        Function function = expressions.get(l);
                        newExpressions.put(l, function == null ? new Function(l) : function);
                    });
            expressions = newExpressions;
        }
    }

    public void calculate(String parameters) {
        parameters = parameters.trim();
        Map<String, Double> params = new HashMap<>();
        String[] pparams = parameters.split(" ");
        for (String p :pparams) {
            String[] par = p.split("=");

            if (par.length != 2)
                logger.error("Invalid input: {}",p);
            else {
                try {
                    params.put(par[0], Double.parseDouble(par[1]));
                } catch (NumberFormatException e) {
                    logger.error("Error parsing '{}' as double", par[1]);
                }
            }
        }

        calculateWithParams(params);
    }

    public void calculateWithParams(final Map<String, Double> parameters) {
        logger.info("====================================");
        logger.info("Calculate with params {}", parameters);

        Set<String> params = parameters.keySet();
        synchronized (expressions) {
            expressions.keySet().stream().forEach(
                    k -> {
                        if (expressions.get(k).canBeCalculated(params))
                            logger.info("{}: {}", k, expressions.get(k).calculate(parameters));
                        else logger.error("{}: value cant be calculated", k);
                    });
        }
    }
}
